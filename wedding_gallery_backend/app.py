from os import urandom

from decouple import config
from flask import Flask, jsonify

app = Flask("wedding_gallery", instance_relative_config=True)

DEFAULT_CONFIG = {
    "DEBUG": config("DEBUG", True, cast=bool),
    "ENV": config("ENV", "development"),
    "SECRET_KEY": urandom(32),
    "JWT_SECRET_KEY": urandom(16),
}

app.config.from_mapping(DEFAULT_CONFIG)

from auth import router_register as auth_router_register

auth_router_register(app)

from gallery import router_register as gallery_router_register

gallery_router_register(app)


@app.route("/api/v1/health")
def health():
    return jsonify(message="running")


if __name__ == "__main__":
    app.run()
