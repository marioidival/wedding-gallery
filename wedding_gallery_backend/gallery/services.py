from bson import ObjectId
from flask import jsonify

from bucket import presigned_urls
from cache import getting_from_cache, save_to_cache
from common import save_photo, validate_body
from mongo import GalleryRepository

BODY_VALIDATORS = {
    "new_gallery": ["galleryName"],
    "add_owner": ["coupleId"],
    "invite": ["friendId"],
    "comment": ["comment", "photoPath"],
    "upload": ["file"],
    "like": ["photoPath"],
    "unlike": ["photoPath"],
}


def _check_permission(uid, gallery):
    owner_list = []
    owner_list.extend(gallery.get("friends", []))
    owner_list.extend(gallery["owners"])
    if ObjectId(uid) not in owner_list:
        raise AssertionError("user not allowed to see photos")


def new_gallery_service(db, json_body, uid):
    if not validate_body(json_body, BODY_VALIDATORS["new_gallery"]):
        return (400, "field 'galleryName' not found on body request")

    gallery_name = json_body["galleryName"]

    result = GalleryRepository.create(db, uid, gallery_name)
    return (201, str(result.inserted_id))


def add_owner_service(db, json_body, gallery_id, owner_id):
    if not validate_body(json_body, BODY_VALIDATORS["add_owner"]):
        return (400, "field 'coupleId' not found on body request")

    couple_id = json_body["coupleId"]
    result = GalleryRepository.add_owner(db, gallery_id, owner_id, couple_id)
    if not result:
        return (404, "gallery not found")
    return (200, "user invited to gallery as owner")


def invite_to_gallery_service(db, json_body, gallery_id, owner_id):
    if not validate_body(json_body, BODY_VALIDATORS["invite"]):
        return (400, "field 'friendId' not found on body request")

    friend_id = json_body["friendId"]
    result = GalleryRepository.invite_to_gallery(db, gallery_id, owner_id, friend_id)
    if not result:
        return (404, "gallery not found")
    return (200, "user invited to gallery")


def approved_photos_service(db, gallery_id, user_id):
    cached_items = getting_from_cache(f"{gallery_id}_approved")
    if cached_items:
        return (200, cached_items)

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    if not gallery:
        return (400, "gallery not found")

    try:
        _check_permission(user_id, gallery)
    except AssertionError as e:
        return (403, str(e))

    return (200, gallery.get("photos", []))


def approve_photos_service(db, gallery_id, user_id, photo_index):
    gallery = GalleryRepository.find_by_id(db, gallery_id)
    if not gallery:
        return (404, "gallery not found")

    if ObjectId(user_id) not in gallery["owners"]:
        return (403, "user unauthorized to get approve photos")

    try:
        photo = gallery["pendingPhotos"].pop(photo_index)
    except (IndexError, KeyError):
        return (404, "photo not found")

    GalleryRepository.approve_photo(db, gallery_id, user_id, photo)

    _cache_photos(db, gallery_id, all=True)
    return (200, "approved photo from gallery")


def pending_photos_service(db, gallery_id, user_id):
    cached_items = getting_from_cache(f"{gallery_id}_pending")
    if cached_items:
        return (200, cached_items)

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    if not gallery:
        return (404, "gallery not found")

    if user_id not in gallery["owners"]:
        return (403, "user unauthorized to get pending photos")

    return (200, gallery.get("pendingPhotos", []))


def comment_photo_service(db, gallery_id, user_id, json_body):
    if not validate_body(json_body, BODY_VALIDATORS["comment"]):
        return (400, "field 'comment' not found on body request")

    comment = json_body["comment"]
    photo_path = json_body["photoPath"]

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    if not gallery:
        return (404, "gallery not found")

    try:
        _check_permission(user_id, gallery)
    except AssertionError as e:
        return (403, str(e))

    result = GalleryRepository.comment_photo(db, gallery_id, user_id, photo_path, comment)
    if not result:
        return (404, "photo not found by index")

    _cache_photos(db, gallery_id, all=True)
    return (200, "")


def like_photo_service(db, gallery_id, user_id, json_body):
    if not validate_body(json_body, BODY_VALIDATORS["like"]):
        return (400, "field 'photoPath' not found on body request")

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    try:
        _check_permission(user_id, gallery)
    except AssertionError as e:
        return (403, str(e))

    photo_path = json_body["photoPath"]
    result = GalleryRepository.like_photo(db, gallery_id, photo_path)
    if not result:
        return (404, "photo not found by index")

    _cache_photos(db, gallery_id, all=True)
    return (200, "")


def unlike_photo_service(db, gallery_id, user_id, json_body):
    if not validate_body(json_body, BODY_VALIDATORS["like"]):
        return (400, "field 'photoPath' not found on body request")

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    try:
        _check_permission(user_id, gallery)
    except AssertionError as e:
        return (403, str(e))

    photo_path = json_body["photoPath"]
    result = GalleryRepository.unlike_photo(db, gallery_id, photo_path)
    if not result:
        return (404, "photo not found by index")

    _cache_photos(db, gallery_id, all=True)
    return (200, "")


def _cache_photos(db, gallery_id, all=False):
    gallery = GalleryRepository.find_by_id(db, gallery_id)
    photos = {}
    photos["approved"] = gallery.get("photos", [])
    if all:
        photos["pending"] = gallery.get("pendingPhotos", [])

    approved = presigned_urls(photos["approved"])
    r = save_to_cache(f"{gallery_id}_approved", approved)
    pending = presigned_urls(photos["pending"])
    r = save_to_cache(f"{gallery_id}_pending", pending)
    return


def upload_photo_services(db, gallery_id, user_id, files):
    if not validate_body(files, BODY_VALIDATORS["upload"]):
        return (400, "field 'file' not found on body request")

    gallery = GalleryRepository.find_by_id(db, gallery_id)
    try:
        _check_permission(user_id, gallery)
    except AssertionError as e:
        return (403, str(e))

    f = files["file"]
    photo_path, has_error = save_photo(f)
    if has_error:
        return (500, f"error to upload photo to bucket: {has_error}")

    result = GalleryRepository.upload_photo(db, gallery_id, photo_path)
    if not result.matched_count:
        return (404, "gallery not found to upload photo")

    _cache_photos(db, gallery_id, all=True)
    return (200, "uploaded photo to gallery")
