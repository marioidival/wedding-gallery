from flask import Blueprint, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required

from mongo import db

from .services import (
    add_owner_service,
    approve_photos_service,
    approved_photos_service,
    comment_photo_service,
    invite_to_gallery_service,
    like_photo_service,
    new_gallery_service,
    pending_photos_service,
    unlike_photo_service,
    upload_photo_services,
)

bp = Blueprint("gallery", __name__)


@bp.route("gallery", methods=["POST"])
@jwt_required()
def new_gallery():
    user_id = get_jwt_identity()
    body = request.json
    status, gallery_id = new_gallery_service(db, body, user_id)
    return jsonify(msg="Gallery created!", _id=str(gallery_id)), status


@bp.route("gallery/<gallery_id>/owners", methods=["PUT"])
@jwt_required()
def update_owners(gallery_id):
    user_id = get_jwt_identity()
    body = request.json
    status, msg = add_owner_service(db, body, gallery_id, user_id)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/invite", methods=["POST"])
@jwt_required()
def invite_to_gallery(gallery_id):
    user_id = get_jwt_identity()
    body = request.json
    status, msg = invite_to_gallery_service(db, body, gallery_id, user_id)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/photos/upload", methods=["POST"])
@jwt_required()
def upload_photo_to_gallery(gallery_id):
    files = request.files
    user_id = get_jwt_identity()
    status, msg = upload_photo_services(db, gallery_id, user_id, files)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/photos", methods=["GET"])
@jwt_required()
def photos(gallery_id):
    """Return a list of all approved photos"""
    user_id = get_jwt_identity()
    status, photos = approved_photos_service(db, gallery_id, user_id)
    return jsonify(photos=photos), status


@bp.route("gallery/<gallery_id>/photos/pendings", methods=["GET"])
@jwt_required()
def pending_photos(gallery_id):
    user_id = get_jwt_identity()
    photos = pending_photos_service(db, gallery_id, user_id)
    return jsonify(photos=photos), 200


@bp.route("gallery/<gallery_id>/photos/approve/<int:photo_id>", methods=["POST"])
@jwt_required()
def approve_photo(gallery_id, photo_id):
    user_id = get_jwt_identity()
    status, msg = approve_photos_service(db, gallery_id, user_id, photo_id)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/photos/comment", methods=["POST"])
@jwt_required()
def comment_photo(gallery_id):
    user_id = get_jwt_identity()
    body = request.json
    status, msg = comment_photo_service(db, gallery_id, user_id, body)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/photos/like", methods=["POST"])
@jwt_required()
def like_photo(gallery_id):
    user_id = get_jwt_identity()
    json_body = request.json
    status, msg = like_photo_service(db, gallery_id, user_id, json_body)
    return jsonify(msg=msg), status


@bp.route("gallery/<gallery_id>/photos/unlike", methods=["POST"])
@jwt_required()
def unlike_photo(gallery_id):
    user_id = get_jwt_identity()
    json_body = request.json
    status, msg = unlike_photo_service(db, gallery_id, user_id, json_body)
    return jsonify(msg=msg), status
