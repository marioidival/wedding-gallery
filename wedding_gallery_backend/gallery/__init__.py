from .routes import bp


def router_register(app):
    app.register_blueprint(bp, url_prefix="/api/v1")


__all__ = ["bp", "router_register"]
