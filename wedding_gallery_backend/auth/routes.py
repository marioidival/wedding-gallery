from flask import Blueprint, abort, g, jsonify, request
from flask_jwt_extended import jwt_required

from mongo import db

from .services import login_service, logout_service, signup_service, validate_body

bp = Blueprint("auth", __name__)


@bp.route("login", methods=["POST"])
def login():
    body = request.json
    result = login_service(db, body)
    if not result:
        return jsonify(msg="user not found or wrong credentials"), 403
    return jsonify(access_token=result), 200


@bp.route("logout", methods=["DELETE"])
@jwt_required()
def logout():
    logout_service(db)
    return jsonify(msg="token revoked"), 200


@bp.route("signup", methods=["POST"])
def signup():
    body = request.json
    user_id = signup_service(db, body)
    return jsonify(msg="User created", _id=str(user_id)), 201
