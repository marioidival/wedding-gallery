from flask_jwt_extended import JWTManager

from mongo import TokenRepository, UserRepository, db

from .routes import bp


def router_register(app):
    app.register_blueprint(bp, url_prefix="/api/v1")
    jwt = JWTManager(app)

    @jwt.user_identity_loader
    def user_id_lookup(user_id):
        return user_id

    @jwt.user_lookup_loader
    def user_lookup_cb(_jwt_header, jwt_data):
        identity = jwt_data["sub"]
        return UserRepository.find_by_id(db, identity)

    @jwt.token_in_blocklist_loader
    def token_is_revoked(jwt_header, jwt_payload):
        jti = jwt_payload["jti"]
        return TokenRepository.find(db, jti) is not None


__all__ = ["bp", "router_register"]
