import hashlib
import os

from flask import abort
from flask_jwt_extended import create_access_token, get_jwt

from common import validate_body
from mongo import TokenRepository, UserRepository

BODY_VALIDATORS = {
    "login": ["username", "password"],
    "signup": ["first_name", "last_name", "username", "password"],
}


def _generate_hash_pswd(text: str, salt: bytes = None):
    if not salt:
        salt = os.urandom(32)

    return hashlib.pbkdf2_hmac("sha256", text.encode("utf-8"), salt, 50000)


def _compare_pswd(pswd: bytes, pswd_to_compare: str, salt: bytes) -> bool:
    return pswd == _generate_hash_pswd(pswd_to_compare, salt)


def login_service(db, body):
    if not validate_body(body, BODY_VALIDATORS["login"]):
        abort(400)

    user = UserRepository.find_by_username(db, body["username"])
    if not user:
        return None

    if _compare_pswd(user["password"], body["password"], user["salt"]):
        return create_access_token(identity=str(user["_id"]))
    return None


def signup_service(db, body) -> str:
    if not validate_body(body, BODY_VALIDATORS["signup"]):
        abort(400)

    salt = os.urandom(32)
    body["password"] = _generate_hash_pswd(body["password"], salt)
    body["salt"] = salt
    return UserRepository.create(db, **body)


def logout_service(db):
    jti = get_jwt()["jti"]
    return TokenRepository.create(db, jti)
