import json
from datetime import timedelta

import redis


def save_to_cache(identifier, items):
    r = redis.Redis()
    return r.setex(identifier, timedelta(minutes=5), json.dumps(items))


def getting_from_cache(identifier):
    r = redis.Redis()
    if r.exists(identifier):
        return json.loads(r.get(identifier))
    return []
