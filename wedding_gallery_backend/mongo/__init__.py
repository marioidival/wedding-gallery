from decouple import config
from pymongo import MongoClient

from .gallery_repository import GalleryRepository
from .token_repository import TokenRepository
from .user_repository import UserRepository

mongo_client = MongoClient(
    config(
        "MONGO_URI",
        "mongodb+srv://mario:anchor@weddinggallerydatabase.hksm8.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    )
)
db = mongo_client["wedding-gallery"]


__all__ = ["GalleryRepository", "UserRepository", "TokenRepository", "db"]
