from bson import ObjectId


class GalleryRepository:
    @staticmethod
    def create(db, uid, name):
        return db.gallery.insert_one({"name": name, "owners": [ObjectId(uid)]})

    @staticmethod
    def find_by_id(db, gid):
        return db.gallery.find_one({"_id": ObjectId(gid)})

    @staticmethod
    def add_owner(db, gid, owner_id, uid):
        return db.gallery.update_one(
            {"_id": ObjectId(gid), "owners": ObjectId(owner_id)},
            {"$push": {"owners": ObjectId(uid)}},
        )

    @staticmethod
    def invite_to_gallery(db, gid, owner_id, uid):
        return db.gallery.update_one(
            {"_id": ObjectId(gid), "owners": ObjectId(owner_id)},
            {"$push": {"friends": ObjectId(uid)}},
        )

    @staticmethod
    def upload_photo(db, gid, photo_path):
        return db.gallery.update_one(
            {"_id": ObjectId(gid)},
            {"$push": {"pendingPhotos": {"path": photo_path, "comments": [], "likes": 0}}},
        )

    @staticmethod
    def like_photo(db, gid, photo_path):
        return db.gallery.update_one(
            {"_id": ObjectId(gid)},
            {"$inc": {"photos.$[photo].likes": 1}},
            array_filters=[{"photo.path": photo_path}],
        )

    @staticmethod
    def unlike_photo(db, gid, photo_path):
        return db.gallery.update_one(
            {"_id": ObjectId(gid)},
            {"$inc": {"photos.$[photo].likes": -1}},
            array_filters=[{"photo.path": photo_path}],
        )

    @staticmethod
    def comment_photo(db, gid, uid, photo_path, comment):
        return db.gallery.update_one(
            {"_id": ObjectId(gid)},
            {"$push": {"photos.$[photo].comments": comment}},
            array_filters=[{"photo.path": photo_path}],
        )

    @staticmethod
    def approve_photo(db, gid, owner_id, photo):
        return db.gallery.update_one(
            {"_id": ObjectId(gid), "owners": ObjectId(owner_id)},
            {
                "$push": {"photos": photo},
                "$pull": {"pendingPhotos": {"path": photo["path"]}},
            },
        )
