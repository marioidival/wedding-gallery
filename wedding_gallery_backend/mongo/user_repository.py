from bson import ObjectId


class UserRepository:
    @staticmethod
    def create(db, first_name, last_name, username, password, salt):
        return db.users.insert(
            {
                "username": username,
                "first_name": first_name,
                "last_name": last_name,
                "password": password,
                "salt": salt,
            }
        )

    @staticmethod
    def find_by_username(db, username):
        return db.users.find_one({"username": username})

    @staticmethod
    def find_by_id(db, uid):
        return db.users.find_one({"_id": ObjectId(uid)})
