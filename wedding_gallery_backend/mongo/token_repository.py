class TokenRepository:
    @staticmethod
    def create(db, token):
        return db.tokens.insert({"jti": token})

    @staticmethod
    def find(db, token):
        return db.tokens.find_one({"jti": token})
