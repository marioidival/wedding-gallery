import os

from decouple import config
from werkzeug.utils import secure_filename

from bucket import upload_file


def validate_body(json_body, validate_fields) -> bool:
    try:
        return _validate_body(json_body, validate_fields)
    except KeyError:
        return False


def _validate_body(jb, fields):
    def _check(field):
        return jb and jb.get(field) and jb[field]

    return all(map(_check, fields))


def save_photo(file_object):
    upload_folder = config("PHOTOS_FOLDER", "/tmp/photos/")
    file_path = os.path.join(upload_folder, secure_filename(file_object.filename))
    file_object.save(file_path)
    result = upload_file(file_path)
    return (file_path, result)
