import boto3
from botocore.config import Config
from botocore.exceptions import ClientError
from decouple import config

BUCKET = config("BUCKET", "wedding-gallery-anchortest")


def upload_file(file_path):
    s3client = boto3.client("s3")
    try:
        return s3client.upload_file(file_path, BUCKET, file_path)
    except ClientError as e:
        return str(e)


def presigned_urls(photos):
    s3client = boto3.client("s3", config=Config(region_name="us-east-2", signature_version="s3v4"))
    urls = []
    for photo in photos:
        resp = s3client.generate_presigned_url(
            "get_object",
            Params={"Bucket": BUCKET, "Key": photo["path"]},
            ExpiresIn=3600,
        )
        photo["url"] = resp
        urls.append(photo)
    return urls
